# syntax=docker/dockerfile:1

# We need a webserver
FROM nginx:stable

# delete the existing default content
RUN rm -rf /usr/share/nginx/html/*

# Copy the built app over
COPY dist /usr/share/nginx/html

# Copy a favicon over
COPY favicon.ico /usr/share/nginx/html

# Copy our modified config over
COPY nginx/default.conf /etc/nginx/conf.d/default.conf

# Expose port 80
EXPOSE 80

# Signal required on shutdown
STOPSIGNAL SIGQUIT

# Start nginx
CMD ["nginx", "-g", "daemon off;"]