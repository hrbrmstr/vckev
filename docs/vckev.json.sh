#!/usr/bin/env bash

ZIPFILE=$(mktemp)

CURL=$(which curl)
JQ=$(which jq)

AWS_RESP=$("${CURL}" -k --silent --cookie "token=${VULNCHECK_API_KEY}" --header 'Accept: application/json' --url "https://api.vulncheck.com/v3/backup/vulncheck-kev")
AWS_URL=$(echo "${AWS_RESP}" | "${JQ}" -r '.data[].url')

"${CURL}" -k --silent --output "${ZIPFILE}" --url "${AWS_URL}"

unzip -p "${ZIPFILE}"
