let { WebR } = await import("https://webr.r-wasm.org/latest/webr.mjs");

export let webR = new WebR();

await webR.init();

/**
 * I did this vs. rely on Observable stdlib's `DOM` being around.
 * @param {double} width 
 * @param {double} height 
 * @param {double} dpi 
 * @returns {CanvasRenderingContext2D}
 */
export function context2d(width, height, dpi) {
  if (dpi == null) dpi = devicePixelRatio;
  var canvas = document.createElement("canvas");
  canvas.width = width * dpi;
  canvas.height = height * dpi;
  canvas.style.width = width + "px";
  var context = canvas.getContext("2d");
  context.scale(dpi, dpi);
  return context;
}
