---
theme: midnight
toc: true
style: style.css
---

# VulnCheck KEV

```js
import { webR } from './components/R.js'
import { circlePack } from "./components/vis.js"
```

```js
const kev = await FileAttachment("vckev.json").json()
const tags = await FileAttachment("tags.json").json()
```

<div class="card">

<h1>Explore VulnCheck KEV CVEs</h1>

<div style="display: inline-block">
  <div style="display: inline-block">

```js
const vulnsNotInKEV = view(Inputs.checkbox(["Not in CISA KEV"], {label: ""}));
```

  </div>
  <div style="display: inline-block"> 

```js
const vckev = kev.filter(d => 
  vulnsNotInKEV.length == 0 ? true : !Object.keys(d).includes("cisa_date_added")
);

const searchVulns = view(Inputs.search(vckev, {
  placeholder: "Search for a vulnerability"
}))
```
  
  </div>
</div>


```js
const selectedVuln = view(Inputs.table(searchVulns.map(d => ({
  cve: d.cve,
  kev_date: d.cisa_date_added ? d.cisa_date_added.substr(0, 10) : "",
  vc_date: d.date_added.substr(0, 10),
  vuln: d.vulnerabilityName
})), {
  layout: "auto",
  multiple: false,
  requried: false
}))

```

<div class="card">

 ${selectedVuln !== null ? vulnInfo(selectedVuln) : ""}

</div>


```js
function vulnInfo(vuln) {

  const v = kev.find(d => d.cve === vuln.cve);

  console.log(v);

  const t = tags.find(tag => tag.cves.includes(v.cve[0]));

  const k = (Object.keys(v).includes("cisa_date_added"))

  const ransom = v.knownRansomwareCampaignUse == "Known" ? "😈" : ""
  const hasTag = t !== undefined ? html`<a target="_blank-gn" href="https://viz.greynoise.io/tags/${t.slug}">🏷️</a>` : ""
  const inKEV = k ? html`<span class="vulncheck-xdb">KEV</span>` : ""
  const kevAdded = k ? html`<br/>KEV: ${v.cisa_date_added.substring(0, 10)}` : ""

  return html`
<h1>${v.cve[0]} <span style='font-size:12pt'> ${ransom} ${hasTag} ${inKEV}</span></h1>
<p><span class='name'><b>${v.vulnerabilityName}</b></span></p>
<p><span class='description'><i>${v.shortDescription}</i></span></p>
<p>${v.vendorProject} / ${v.product}</span></p>
<p><span class='date-added'>VC Added: ${v.date_added.substr(0, 10)}${kevAdded}</span></p>
`

}
```

</div>

```js
const deltas = kev.filter(d => Object.keys(d).includes("cisa_date_added")).map(d => 
  ({ delta: Math.ceil(
    (new Date(d.date_added) - new Date(d.cisa_date_added)) / (1000 * 60 * 60 * 24)
  ) })
);
```

<div class="card">

<h1># Days VulnCheck Is Ahead Of KEV</h1>

```js
Plot.plot({
  width: width,
  x: { label: "Delta (days)" },
  y: {grid: true},
  marks: [
    Plot.rectY(deltas, Plot.binX({y: "count", thresholds: 100}, {x: "delta", tip: true })),
    Plot.ruleY([0]),
  ]
})
```

</div>

<div class="card">

<h1>VulnCheck Vulnerability Inventory Trajectory</h1>

```js
Plot.plot({
  width: width,
  x: { label: null },
  y: {grid: true, label: "# Vulns" },
  marks: [
    Plot.line(cumDates, { x: "date_added", y: "csum", tip: true}),
    Plot.ruleY([0]),
  ]
})
```

</div>


```js
const cumDatesR = await webR.evalR(`
date_added |> 
  as.Date() |> 
  sort() |> 
  table(
    dnn = "date_added"
  ) |> 
  as.data.frame.table(
    responseName = "n"
  ) |> 
  transform(
    date_added = as.character(date_added),
    csum = cumsum(n)
  )
`, { env: {
  date_added: kev.map(d => d.date_added)
}})
const cumDates = await cumDatesR.toD3();
cumDates.forEach(d => d.date_added = new Date(d.date_added))
```

<div class="card">

<h1>VulnCheck KEVerse</h1>

<div id="verse">

```js
const kevRollup = d3.rollup(
  kev.map(d => {
    d.n = 1;
    return d
  }),
  D => d3.sum(D, d => d["n"]),
  d => d.vendorProject,
  d => d.product
)

view(circlePack(kevRollup))
```

</div>

</div>


```js
const container = document.getElementById('verse');
let scale = 1;
let panning = false;
let startX, startY;

container.addEventListener('wheel', (event) => {
  event.preventDefault();
  const delta = event.deltaY < 0 ? 1.1 : 0.9;
  scale *= delta;
  container.style.transform = `scale(${scale})`;
});

container.addEventListener('mousedown', (event) => {
  panning = true;
  startX = event.clientX - container.offsetLeft;
  startY = event.clientY - container.offsetTop;
  container.style.cursor = 'grabbing';
});

container.addEventListener('mouseup', () => {
  panning = false;
  container.style.cursor = 'move';
});

container.addEventListener('mousemove', (event) => {
  if (!panning) return;
  const deltaX = event.clientX - container.offsetLeft - startX;
  const deltaY = event.clientY - container.offsetTop - startY;
  container.style.transform = `translate(${deltaX}px, ${deltaY}px) scale(${scale})`;
});
```
