#!/usr/bin/env bash

CURL=$(which curl)
JQ=$(which jq)

"${CURL}" -k \
  --silent \
  --url "https://viz.greynoise.io/api/greynoise/v2/meta/metadata" | "${JQ}" '.metadata'

