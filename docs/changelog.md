---
theme: midnight
toc: true
style: style.css
---

# Changelog

---

## 2024-03-23

- Published initial version
- Added changelog